<?php

namespace Tests\Feature;

use App\Entities\User;
use App\Exceptions\Api\LogicException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;

use Tests\TestCase;

class MarketApiTest extends TestCase
{
    use RefreshDatabase;

    public function test_unauthorized_user_cannot_create_stock()
    {
        $response = $this->json("POST", "/api/stocks", [
                "price" => 23.2,
                "start_date" => '2021-12-01 09:30:15'
            ]);
        $response->assertStatus(401);
    }

    public function test_authorized_user_can_create_stock()
    {
        $stockData = [
            "price" => 23.2,
            "start_date" => '2021-12-01 09:30:15'
        ];
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->json("POST", "/api/stocks", $stockData);
        $response
            ->assertStatus(201)
            ->assertJsonFragment($stockData);
        $this->assertDatabaseHas(
            'stocks',
            array_merge(['id' => 1], $stockData)
        );
    }
    public function test_user_cannot_create_stock_with_negative_price()
    {
        $stockData = [
            "price" => -50,
            "start_date" => '2021-07-22 09:30:15'
        ];
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->json("POST", "/api/stocks", $stockData);
        $response
           ->assertStatus(400)
        ->assertJsonFragment(['price'  => ['The price must be greater than 0.']]);
        $this->assertDatabaseMissing(
            'stocks',
            array_merge(['id' => 1], $stockData)
        );
    }
    public function test_user_cannot_create_stock_without_price()
    {
        $stockData = [
             "start_date" => '2021-07-22 09:30:15'
        ];
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->json("POST", "/api/stocks", $stockData);
        $response
           ->assertStatus(400)
           ->assertJsonFragment(['price'  => ['The price field is required.']]);
        $this->assertDatabaseMissing(
            'stocks',
            array_merge(['id' => 1], $stockData)
        );
    }

    public function test_user_cannot_create_stock_without_start_date()
    {
        $stockData = [
            "price" => 50,
        ];
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->json("POST", "/api/stocks", $stockData);
        $response
           ->assertStatus(400)
        ->assertJsonFragment(['start_date'=>['The start date field is required.']]);
        $this->assertDatabaseMissing(
            'stocks',
            array_merge(['id' => 1], $stockData)
        );
    }
    public function test_user_cannot_create_stock_with_start_date_in_past()
    {
        $stockData = [
            "price" => 50,
            "start_date" => '2020-06-22 09:30:15'
        ];
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->json("POST", "/api/stocks", $stockData);
        $response
           ->assertStatus(400)
           ->assertJsonFragment(['start_date'=>['The start date must be a date after or equal to now.']]);
        $this->assertDatabaseMissing(
            'stocks',
            array_merge(['id' => 1], $stockData)
        );
    }

    public function test_user_cannot_create_stock_with_start_date_in_wrong_format()
    {
        $stockData = [
            "price" => 50,
            "start_date" => '2020-23-22 09:30:15'
        ];
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->json("POST", "/api/stocks", $stockData);
        $response
            ->assertStatus(400)
            ->assertJsonFragment(['The start date does not match the format Y-m-d H:i:s.']);
        $this->assertDatabaseMissing(
            'stocks',
            array_merge(['id' => 1], $stockData)
        );
    }

    public function test_user_cannot_create_stock_with_start_date_in_wrong_format2()
    {
        $stockData = [
            "price" => 50,
            "start_date" => '22-02-2022 09:30:15'
        ];
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->json("POST", "/api/stocks", $stockData);
        $response
            ->assertStatus(400)
            ->assertJsonFragment(['The start date does not match the format Y-m-d H:i:s.']);
        $this->assertDatabaseMissing(
            'stocks',
            array_merge(['id' => 1], $stockData)
        );
    }
    public function test_authorized_user_can_delete_own_post()
    {
        $stockData = [
            "price" => 23.2,
            "start_date" => '2021-07-01 09:30:15'
        ];
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->json("POST", "/api/stocks", $stockData);
        $this->assertDatabaseHas(
            'stocks',
            array_merge(['id' => 1], $stockData)
        );
        $response = $this->actingAs($user)->json("DELETE", "/api/stocks/1");
        $response
            ->assertStatus(204);
        $this->assertDatabaseMissing(
            'stocks',
            array_merge(['id' => 1], $stockData)
        );
    }

    public function test_authorized_user_can_delete_own_post2()
    {
        $stockData = [
            "price" => 23.2,
            "start_date" => '2021-07-01 09:30:15'
        ];
        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $response = $this->actingAs($user2)->json("POST", "/api/stocks", $stockData);
        $this->assertDatabaseHas(
            'stocks',
            array_merge(['id' => 1], $stockData)
        );
        $response = $this->actingAs($user2)->json("DELETE", "/api/stocks/1");
        $response
            ->assertStatus(204);
        $this->assertDatabaseMissing(
            'stocks',
            array_merge(['id' => 1], $stockData)
        );
    }

    public function test_authorized_user_cannot_delete_post_of_another_user()
    {
        $stockData = [
            "price" => 23.2,
            "start_date" => '2021-07-01 09:30:15'
        ];
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->json("POST", "/api/stocks", $stockData);
        $user1= factory(User::class)->create();
        $response = $this->actingAs($user1)->json("DELETE", "/api/stocks/1");
        $response
            ->assertStatus(404);
        $this->assertDatabaseHas(
            'stocks',
            array_merge(['id' => 1], $stockData)
        );
    }

    public function test_unauthorized_user_cannot_delete_post()
    {
        $stockData = [
            "price" => 23.2,
            "start_date" => '2021-07-01 09:30:15'
        ];
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->json("POST", "/api/stocks", $stockData);
        Auth::logout();// if you use actingAs once you login us this user during all current test
        $this->assertGuest();
        $response = $this->json("DELETE", "/api/stocks/1");
        $response
            ->assertStatus(401);
        $this->assertDatabaseHas(
            'stocks',
            array_merge(['id' => 1], $stockData)
        );
    }
    public function test_unauthorized_user_cannot_delete_post2()
    {
        $response = $this->json("DELETE", "/api/stocks/1");
        $response
            ->assertStatus(401);
    }

    public function test_unauthorized_user_can_see_analytics()
    {
        $stockData = [
            "price" => 30,
            "start_date" => '2021-07-03 09:30:15'
        ];
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->json("POST", "/api/stocks", $stockData);
        $stockData = [
            "price" => 10,
            "start_date" => '2021-07-03 09:31:20'
        ];
        $response = $this->actingAs($user)->json("POST", "/api/stocks", $stockData);
        $stockData = [
            "price" => 20,
            "start_date" => '2021-07-03 09:33:55'
        ];
        $response = $this->actingAs($user)->json("POST", "/api/stocks", $stockData);
        Auth::logout();
        $analyticData = [
            "start_date"=>  (new \DateTime('2021-07-01 01:00:15'))->getTimestamp(),
            "end_date"=>   (new \DateTime('2021-07-08 09:30:36'))->getTimestamp(),
            "frequency"=> 24*60*60,
        ];
        $response = $this->get("/api/chart-data?start_date={$analyticData['start_date' ]}&end_date={$analyticData['end_date' ]}&frequency={$analyticData['frequency' ]}");
         $response
          ->assertStatus(200);
        $this->assertCount(1, $response->getData()->data);
        $this->assertEquals(20.0, $response->getData()->data[0]->price);
    }

    public function test_get_logicException_if_frequency_less_zero()
    {
        $this->expectException(LogicException::class);
        $stockData = [
            "price" => 20,
            "start_date" => '2022-06-02 10:21:15'
        ];
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->json("POST", "/api/stocks", $stockData);
        $analyticData = [
            'start_date' => (new \DateTime('2022-05-02 09:00:15'))->getTimestamp(),
            'end_date' => (new \DateTime('2022-07-01 09:30:36'))->getTimestamp(),
            'frequency' => -3600,
        ];
        $response = $this->get(
            "/api/chart-data*?start_date={$analyticData['start_date' ]}&end_date={$analyticData['end_date' ]}&frequency={$analyticData['frequency' ]}"
        );
    }
}
