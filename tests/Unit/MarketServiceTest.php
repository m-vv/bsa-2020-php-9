<?php

namespace Tests\Unit;

use App\Entities\Stock;
use App\Repositories\StockRepository;
use App\Services\Exceptions\InvalidFrequencyException;
use Illuminate\Support\Collection;
use Tests\TestCase;
use App\Services\MarketDataService;

class MarketServiceTest extends TestCase
{
    public function test_getChartData_returns_data_correctly_by_hour()
    {
        $mockVal = [];
        $t = new Stock();
        $t->price = 18;
        $t->start_date =  \DateTime::createFromFormat('Y-m-d H:i:s', '2020-05-01 11:01:20');
        $mockVal[] = $t;
        $t1 = new Stock();
        $t1->price = 8;
        $t1->start_date =  \DateTime::createFromFormat('Y-m-d H:i:s', '2020-05-01 12:30:20');
        $mockVal[] = $t1;
        $t2 = new Stock();
        $t2->price = 12;
        $t2->start_date =  \DateTime::createFromFormat('Y-m-d H:i:s', '2020-05-01 12:31:20');
        $mockVal[] = $t2;
        $repoStub = $this->createStub(StockRepository::class);
        $repoStub->method('findByCriteria')->willReturn(new Collection($mockVal));
        $dataService = new MarketDataService($repoStub);
        $startDate = \DateTime::createFromFormat('Y-m-d H:i:s', '2020-05-01 10:00:00');
        $endDate = \DateTime::createFromFormat('Y-m-d H:i:s', '2020-05-01 17:00:00');
        $res = $dataService->getChartData($startDate, $endDate, 60*60);
        $this->assertCount(2, $res);
        $this->assertEquals(10.0, $res[1]->getPrice());
    }

    public function test_getChartData_returns_data_correctly_by_day()
    {
        $mockVal = [];
        $t = new Stock();
        $t->price = 18;
        $t->start_date =  \DateTime::createFromFormat('Y-m-d H:i:s', '2020-05-01 11:01:20');
        $mockVal[] = $t;
        $t1 = new Stock();
        $t1->price = 9;
        $t1->start_date =  \DateTime::createFromFormat('Y-m-d H:i:s', '2020-05-01 12:30:20');
        $mockVal[] = $t1;
        $t2 = new Stock();
        $t2->price = 12;
        $t2->start_date =  \DateTime::createFromFormat('Y-m-d H:i:s', '2020-05-01 12:31:20');
        $mockVal[] = $t2;
        $repoStub = $this->createStub(StockRepository::class);
        $repoStub->method('findByCriteria')->willReturn(new Collection($mockVal));
        $dataService = new MarketDataService($repoStub);
        $startDate = \DateTime::createFromFormat('Y-m-d H:i:s', '2020-05-01 10:00:00');
        $endDate = \DateTime::createFromFormat('Y-m-d H:i:s', '2020-05-22 17:00:00');
        $res = $dataService->getChartData($startDate, $endDate, 24*60*60);
        $this->assertCount(1, $res);
        $this->assertEquals(13.0, $res[0]->getPrice());
    }
    public function test_getChartData_returns_data_correctly_by_week()
    {
        $mockVal = [];
        $t = new Stock();
        $t->price = 18;
        $t->start_date =  \DateTime::createFromFormat('Y-m-d H:i:s', '2020-05-01 11:01:20');
        $mockVal[] = $t;
        $t1 = new Stock();
        $t1->price = 9;
        $t1->start_date =  \DateTime::createFromFormat('Y-m-d H:i:s', '2020-05-11 10:30:20');
        $mockVal[] = $t1;
        $t2 = new Stock();
        $t2->price = 12;
        $t2->start_date =  \DateTime::createFromFormat('Y-m-d H:i:s', '2020-06-01 12:31:20');
        $mockVal[] = $t2;
        $repoStub = $this->createStub(StockRepository::class);
        $repoStub->method('findByCriteria')->willReturn(new Collection($mockVal));
        $dataService = new MarketDataService($repoStub);
        $startDate = \DateTime::createFromFormat('Y-m-d H:i:s', '2020-05-01 10:00:00');
        $endDate = \DateTime::createFromFormat('Y-m-d H:i:s', '2020-07-22 17:00:00');
        $res = $dataService->getChartData($startDate, $endDate, 7*24*60*60);
        $this->assertCount(3, $res);
        $this->assertEquals(12.0, $res[2]->getPrice());
    }

    public function test_getChartData_returns_data_correctly_if_empty_data()
    {
        $repoStub = $this->createStub(StockRepository::class);
        $repoStub->method('findByCriteria')->willReturn(new Collection());
        $dataService = new MarketDataService($repoStub);
        $startDate = \DateTime::createFromFormat('Y-m-d H:i:s', '2020-07-22 15:05:00');
        $endDate = \DateTime::createFromFormat('Y-m-d H:i:s', '2020-07-23 17:00:00');
        $res = $dataService->getChartData($startDate, $endDate, 60);
        $this->assertCount(0, $res);
    }

    public function test_get_InvalidFrequencyException_frequency_equal_zero()
    {
        $this->expectException(InvalidFrequencyException::class);
        $repository = $this->createMock(StockRepository::class);
        $dataService = $this->app->makeWith('\App\Services\MarketDataService', ['stockRepository'=> $repository ]);
        $startDate = \DateTime::createFromFormat('Y-m-d H:i:s', '2020-05-01 11:22:20');
        $endDate = \DateTime::createFromFormat('Y-m-d H:i:s', '2020-05-01 11:45:20');
        $res = $dataService->getChartData($startDate, $endDate, 0);
    }

    public function test_get_InvalidFrequencyException_frequency_less_than_zero()
    {
        $this->expectException(InvalidFrequencyException::class);
        $repository = $this->createMock(StockRepository::class);
        $dataService = new MarketDataService($repository);
        $startDate = \DateTime::createFromFormat('Y-m-d H:i:s', '2020-05-01 11:22:20');
        $endDate = \DateTime::createFromFormat('Y-m-d H:i:s', '2020-05-01 11:45:20');
        $res = $dataService->getChartData($startDate, $endDate, -3);
    }
}
