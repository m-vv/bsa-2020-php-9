<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Actions\CreateStockAction;
use App\Actions\DeleteStockAction;
use App\Actions\GetChartDataAction;
use App\Actions\Requests\CreateStockRequest;
use App\Actions\Requests\DeleteStockRequest;
use App\Actions\Requests\GetChartDataRequest;
use Illuminate\Support\Facades\Validator;

class MarketController extends Controller
{
    private CreateStockAction $createStockAction;
    private GetChartDataAction $getChartDataAction;
    private DeleteStockAction $deleteStockAction;

    public function __construct(
        CreateStockAction $createStockAction,
        GetChartDataAction $getChartDataAction,
        DeleteStockAction $deleteStockAction
    ) {
        $this->createStockAction = $createStockAction;
        $this->getChartDataAction = $getChartDataAction;
        $this->deleteStockAction = $deleteStockAction;
    }

    public function createStock(Request $request)
    {
        $validateRules = [
            'price' =>'gt:0|required' ,
            'start_date'=>'required|date_format:Y-m-d H:i:s|after_or_equal:now',
        ];
        $validator = Validator::make($request->all(), $validateRules);
        if ($validator->fails()) {
            return response()->json([
                            "messages" => $validator->errors(),
                            ], 400);
        }

        $result = $this->createStockAction->execute(
            new CreateStockRequest(
                new \DateTime($request->start_date),
                floatval($request->price)
            )
        );

        return response()->json([
            "data" => $result->toArray()
        ], 201);
    }

    public function deleteStock(Request $request)
    {
        $result = $this->deleteStockAction->execute(
            new DeleteStockRequest($request->stockId)
        );

        return response()->json([
            "data" => $result->toArray()
        ], 204);
    }

    public function getChartData(Request $request)
    {
        $validateRules = [
            'start_date'=>'required|date_format:U',
            'end_date'=>'required|date_format:U|after:start_date',
             'frequency' => 'required|integer' //remove  |gt:0' to get exception LogicException
        ];
        $validator = Validator::make($request->input(), $validateRules);
        if ($validator->fails()) {
            return response()->json([
                                        "messages" => $validator->errors(),
                                    ], 400);
        }

        $result = $this->getChartDataAction->execute(
            new GetChartDataRequest(
                new \DateTime('@' . $request->start_date),
                new \DateTime('@' . $request->end_date),
                intval($request->frequency),
            )
        );

        return response()->json([
            "data" => $result->toArray()
        ], 200);
    }
}
